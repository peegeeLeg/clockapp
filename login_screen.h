#ifndef LOGIN_SCREEN_H
#define LOGIN_SCREEN_H

#include <QDialog>
#include <system_user.h>
#include <SystemShare.h>
#include <fingerprintscanner.h>
#include <database_controller.h>
#include <UserAdministration.h>
#include <identification_screen.h>



namespace Ui {
class Login_Screen;
}

class Login_Screen : public QDialog
{
  Q_OBJECT

public:
  explicit Login_Screen(QWidget *parent = 0);
  ~Login_Screen();

  /*!
   * \brief Login_Screen::VerifyCredentials
   * \param Username
   * \param Password
   * \return true if access granted
   *         false if access denied
   */
  //bool VerifyCredentials(QString Username, QString Password);

public slots:
  /*!
   * \brief on_login
   * \param loginState
   * \param typeUser
   */
  void on_login(bool loginState, userType typeUser);


signals:
  void signal_login(bool loginState, userType typeUser);

private slots:
 // void on_ls_pb_login_clicked();

  //void on_ls_pb_forgot_password_clicked();

  //void on_ls_pb_send_email_forgot_password_clicked();

  void on_ls_pb_scan_finger_clicked();

  void on_ls_pb_cancel_login_clicked();

private:
  Ui::Login_Screen *ui;

  // int
  int login_tries;

  // enum
  //userType type_user;
  // Identify  Recipient
  IdentificationScreen*       newIdentificationScreen;
  // input form
  UserAdministration*         newUserAdmin;
  // fingerprint scanner window
 // FingerprintScanner*         newFingerprintScannerWindow;


  // bool states
  bool                        login_state;
  bool                        db_conn_state_;
  // enum
 // Login_Screen                newLoginScreen;
  userType                    user_type;
  // db controller
  DatabaseController*         db_controller_;


  FingerprintScanner* login_finger_scanner;

};

#endif // LOGIN_SCREEN_H
