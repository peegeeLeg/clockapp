#include <fingerprintscanner.h>
#include "ui_fingerprintscanner.h"

#include <QMovie>
#include <QDebug>
#include <QThread>

FingerprintScanner::FingerprintScanner(QWidget *parent) :
  QDialog(parent, Qt::CustomizeWindowHint),
  ui(new Ui::FingerprintScanner)
{
  ui->setupUi(this);


  // Change colour of frame to white
  QPalette pal = this->palette();
  pal.setColor(QPalette::Window, Qt::white);
  this->setPalette(pal);

  // set icons
  ui->fs_pb_scan_finger->setIcon(QIcon(":/Company/Logo/FingerScan.png"));
  ui->fs_pb_scan_finger->setIconSize(ui->fs_pb_scan_finger->size());

  // add loading scene
  QMovie *movie = new QMovie(":/Company/Logo/loading.gif");
  ui->fs_lbl_info_diplsyProgress->setMovie(movie);
  movie->start();

  // set init status
  ui->fs_lbl_scanner_info->setText(warningMsg("Scanner Loadixng..."));



  // set widget
  ui->fs_pb_confirm->setEnabled(false);



  if(initializeScanner()){
    // set init status
    ui->fs_lbl_scanner_info->setText(successMsg("Scanner ready."));
    ui->fs_lbl_info_diplsyProgress->setPixmap(QPixmap::fromImage(QImage(":/Company/Logo/greenTick2.jpg")));
    // set widget
    ui->fs_pb_confirm->setEnabled(true);
  }

}

UFS_STATUS      FingerprintScanner::ufs_res;
HUFScanner      FingerprintScanner::hScanner;
HUFMatcher      FingerprintScanner::hMatcher;
int             FingerprintScanner::TemplateSize;
int             FingerprintScanner::nEnrollQuality;
unsigned char*  FingerprintScanner::Template;

FingerprintScanner::~FingerprintScanner()
{
  delete ui;
}

bool FingerprintScanner::initializeScanner(){

  int nScannerNumber = 0;

  ufs_res = UFS_Init();

  // Check number of scanners

  ufs_res = UFS_GetScannerNumber(&nScannerNumber);

  // If number of scanner is under one, that means there is no scanner in this system
   qDebug()<<"# of scanners connected: "<<nScannerNumber;

  if(nScannerNumber > 0)
  {
    // Get first scanner handle (0 means first scanner)
    ufs_res = UFS_GetScannerHandle(0, &hScanner);

    int value;

    // Set timeout for capturing images to 5 seconds
    value = 20000;
    ufs_res = UFS_SetParameter(hScanner, UFS_PARAM_TIMEOUT, &value);

    // Set template size to 384 bytes
    value = MAX_TEMPLATE_SIZE;
    ufs_res = UFS_SetParameter(hScanner, UFS_PARAM_TEMPLATE_SIZE, &value);

    // Set not to detect core when extracting template
    value = false;
    ufs_res = UFS_SetParameter(hScanner, UFS_PARAM_DETECT_CORE, &value);

    // Security level ranges from 1 to 7
    value = SECURITY_LEVEL;
    UFM_SetParameter(hMatcher, UFM_PARAM_SECURITY_LEVEL, &value);

    // Sensitivity level ranges from 1 to 7
    value = SENSITIVITY_LEVEL;
    UFM_SetParameter(hMatcher, UFS_PARAM_SENSITIVITY, &value);
    // Clear capture buffer

    ufs_res = UFS_ClearCaptureImageBuffer(hScanner);

    Template = new unsigned char[MAX_TEMPLATE_SIZE];

    // set init status
    ui->fs_lbl_scanner_info->setText(successMsg("Scanner ready."));
    ui->fs_lbl_info_diplsyProgress->setPixmap(QPixmap::fromImage(QImage(":/Company/Logo/greenTick2.jpg")));
    // set widget
    ui->fs_pb_confirm->setEnabled(true);
    return true;
  }

  return false;
}

bool FingerprintScanner::capture(QByteArray& fng_template, int& quality)
{

  // Prep to capture
  quality         = 0;
  fng_template    = NULL;
  nEnrollQuality  = 0;

  // Capture single image
  ufs_res = UFS_CaptureSingleImage(hScanner);

  char* bmpFileName = "just_captured.bmp";
  ufs_res = UFS_SaveCaptureImageBufferToBMP(hScanner,bmpFileName);

  // If capturing images is fail, iterate above capture routine or show error message
  // Extract template from captured image
  ufs_res = UFS_Extract(hScanner, Template, &TemplateSize, &nEnrollQuality);

  fng_template  = QByteArray((char*)Template, TemplateSize);
  quality       = nEnrollQuality;

  return true;
}

bool FingerprintScanner::identify(QList<QByteArray> templates,
                                  QByteArray fng_template,
                                  int& index){


  // Create matcher
  UFM_Create(&hMatcher);

  // Set security level to 3
  int value = 3;
  UFM_SetParameter(hMatcher, UFM_PARAM_SECURITY_LEVEL, &value);
  int bVerifySucceed;

  for(int i = 0; i < templates.size(); i++)
  {
    // Verify two templates
    UFM_Verify(hMatcher,
               (unsigned char*)fng_template.data(),
               fng_template.size(),
               (unsigned char*)templates.at(i).data(),
               templates.at(i).size(),
               &bVerifySucceed);

    // check
    if (bVerifySucceed){
      index = i;
      return true;
    }
  }

  index = -1;
  return false;

}

bool FingerprintScanner::verify(QByteArray probe_template,
                                QByteArray stored_templae)
{
  // Create matcher
  UFM_Create(&hMatcher);

  // Set security level to 3
  int value = 3;
  UFM_SetParameter(hMatcher, UFM_PARAM_SECURITY_LEVEL, &value);
  int bVerifySucceed;


  // Verify two templates
  UFM_Verify(hMatcher,
             (unsigned char*)stored_templae.data(),
             stored_templae.size(),
             (unsigned char*)probe_template.data(),
             probe_template.size(),
             &bVerifySucceed);

  // check
  if (bVerifySucceed)
    return true;
  else
    return false;
}

bool FingerprintScanner::abortCapturing()
{
  ufs_res = UFS_AbortCapturing(hScanner);

  if (ufs_res == UFS_OK)
    return true;
  else
    return false;
}

bool FingerprintScanner::isCapturing()
{
  int pbCapturing = 0;

  UFS_IsCapturing(hScanner, &pbCapturing);

  if(pbCapturing == 1)
    return true;
  else
    return false;
}



void FingerprintScanner::on_fs_pb_scan_finger_clicked()
{

}

void FingerprintScanner::on_fs_pb_confirm_clicked()
{

}

void FingerprintScanner::on_fs_pb_cancel_clicked()
{
  // close window
  this->close();
}
