#include <SystemShare.h>


QString getUserTypeToString(userType u_t)
{
  if(u_t == SuperAdmin)
    return QString("Super Administrator");
  else if((u_t == Admin))
    return QString("Administrator");
  else if((u_t == Distributor))
    return QString("Distributor");
  else if((u_t == Recipient))
    return QString("Recipient");
  else
    return QString("generalUser");
}

QString getRecipientTypeToString(recipientType r_t)
{
  if(r_t == Recipient)
    return QString("Recipient");
  else if((r_t == Authorised))
    return QString("Authorised");

}

QString getPackReceiverToString(packReceiver p_r)
{
  if(p_r == SELF)
    return QString("SELF");
  else if (p_r == AUTHORIZED)
    return QString("AUTHORIZED");
  else if (p_r == ISSUER)
    return QString("ISSUER");
  else
    return QString("");
}

packReceiver getStringTopackReceiver(QString p_r)
{
  if(p_r == "SELF")
    return SELF;
  else if (p_r == "AUTHORIZED")
    return AUTHORIZED;
  else if (p_r == "ISSUER")
    return ISSUER;
  else
    return ISSUER;
}

userType getStringToUserType(QString u_t)
{
  if(u_t == "Super Administrator")
    return SuperAdmin;
  else if((u_t == "Administrator"))
    return Admin;
  else if((u_t == "Distributor"))
    return Distributor;
  else
    return generalUser;
}

recipientType getStringToRecipientType(QString r_t)
{
  if(r_t == "Recipient")
    return Recipient;
  else if((r_t == "Authorised"))
    return Authorised;
}

QString getAccStatusToString(accStatus a_s)
{
  if(a_s == Blocked)
    return QString("Blocked");
  else if((a_s == Active))
    return QString("Active");
  else
    return QString("Undefined");
}

accStatus getStringToAccStatus(QString a_s)
{
  if(a_s == "Active")
    return Active;
  else
    return Blocked;
}


QString errorMsg(QString str)
{
  return QString("<font color='red'> %1</font>").arg(str);
}

QString warningMsg(QString str)
{
  return QString("<font color='blue'> %1</font>").arg(str);
}

QString successMsg(QString str)
{
  return QString("<font color='green'> %1</font>").arg(str);
}

QString normalMsg(QString str)
{
  return QString("<font color='black'> %1</font>").arg(str);
}



