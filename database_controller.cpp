#include <QDebug>
#include <SystemShare.h>
#include <database_controller.h>

DatabaseController::DatabaseController()
{
  // init active db handle to a dut
  active_db_handle_ = new Database(INVALID_DB_SYS,"","","","");

//  // create remote db instance
//  remote_db_handle_  = new Database( QMYSQL,
//                                     "11sql27.jnb2.host-h.net",
//                                     "trtnosyurv_db3",
//                                     "trtnosyurv_3",
//                                     "mtHQEre2sK8");
//  // initialise remote server
//  if(remote_db_handle_->InitialiseDb() == 0)
//    qDebug()<<"remote_db_handle_->InitialiseDb(): Success";
//  else
//    qDebug()<<"remote_db_handle_->InitialiseDb(): Failed";


  // create local db instance
  local_db_handle_  = new Database( QSQLITE,"","kedi_db.sqlite","","");

  // initialise local server
  if(local_db_handle_->initialiseDb() == 0)
  {
    qDebug()<<"DatabaseController::DatabaseController: "
              "local_db_handle_->InitialiseDb(): Success";

    //- assign to active db handle
    active_db_handle_ = local_db_handle_;
  }
  else
    qDebug()<<"DatabaseController::DatabaseController: "
              "local_db_handle_->InitialiseDb(): Failed";


}

DatabaseController::~DatabaseController()
{
}

Database*  DatabaseController::local_db_handle_;
Database*  DatabaseController::remote_db_handle_;
Database*  DatabaseController::active_db_handle_;
