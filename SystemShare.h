#ifndef SYSTEMSHARE_H
#define SYSTEMSHARE_H

#include <QString>
#include <QDate>

enum adminAction{
  AddUser    = 1,
  EditUser   = 2,
  DeleteUser = 3,
  AuthenticateUser = 4
};


enum accStatus{
  Blocked = 0,
  Active  = 1
};


/*!
 * \brief The dbSystem enum
 */
enum dbSystem{
  QMYSQL = 1,
  QSQLITE,
  INVALID_DB_SYS
};

/*!
 * \brief The dbAccessState enum
 */
enum dbAccessState{
  REMOTE = 1,
  LOCAL,
  BOTH,
  NONE
};


enum packReceiver{
  SELF = 1,
  AUTHORIZED,
  ISSUER
};

/*!
 * \brief The userType enum
 */
enum userType{
  SuperAdmin  = 1,
  Admin       = 2,
  Distributor = 3,
  generalUser = 4,
  nonUser
};
/*!
 * \brief The recipientType enum
 */
enum recipientType{
  Recipient  = 1,
  Authorised = 2,
  Learner    = 3,
  Group      = 4
};

/*!
 * \brief The dignityPackAction enum
 */
enum dignityPackAction{
  ADD     = 1,
  REMOVE  = 2
};

/*!
 * \brief The reportType enum
 */
enum reportType{
  DistributionDetailsReport     = 1,
  DistributionSummaryReport     = 2,
  SystemUserListReport          = 3,
  RecipientsGroupListingReport  = 4,
  RegionDeliveriesReport        = 5,
  SchoolDeliveriesReport        = 6,
  UnknownReport                 = 7
};

QString getPackReceiverToString(packReceiver p_r);
packReceiver getStringTopackReceiver(QString p_r);

QString getUserTypeToString(userType u_t);
userType getStringToUserType(QString u_t);

QString getRecipientTypeToString(recipientType r_t);
recipientType getStringToRecipientType(QString r_t);

QString getAccStatusToString(accStatus a_s);
accStatus getStringToAccStatus(QString a_s);

QString errorMsg(QString str);
QString warningMsg(QString str);
QString successMsg(QString str);
QString normalMsg(QString str);

/*!
 * \brief The UserDetails struct
 */
struct UserDetails{
  int         id;
  QString     name;
  QString     surname;
  QString     email_address;
  QString     contact_number;
  userType    user_type;
  accStatus   account_status;
  QByteArray  fingerprint_template;
  bool        fingerprint_enrolled;

  UserDetails(){
    clear();
  }

  void clear()
  {
    id                    = 0;
    name.clear();
    surname.clear();
    email_address.clear();
    contact_number.clear();
    account_status        = Blocked;
    user_type             = generalUser;
    fingerprint_template  = NULL;
    fingerprint_enrolled  = false;
  }
};

/*!
 * \brief The RecipientsDetails struct
 */
struct RecipientsDetails{
  int           id;
  int           region_id;
  int           school_id;
  QString       name;
  QString       surname;
  QString       gender;
  QString       address;
  QString       contact_number;
  QString       id_number;
  recipientType recipient_type;
  QDate         birth_date;
  QByteArray    fingerprint_template;
  bool          fingerprint_enrolled;

  RecipientsDetails(){
    clear();
  }
  void clear()
    {
      id                    = 0;
      region_id             = 0;
      school_id             = 0;
      name.clear();
      surname.clear();
      gender.clear();
      address.clear();
      contact_number.clear();
      id_number.clear();
      recipient_type        = Recipient;
      birth_date            = QDate();
      fingerprint_template  = NULL;
      fingerprint_enrolled  = false;

    }

  };


  /*!
   * \brief The staffDetails struct
   */
  struct StaffDetails{
    int           id;
    QString       username;
    QByteArray    fingerprint_template;
    bool          fingerprint_enrolled;

    staffDetails(){
      clear();
    }

  void clear()
  {
    id                    = 0;
    username.clear();
    fingerprint_template  = NULL;
    fingerprint_enrolled  = false;

  }

};

struct RegionDetails{
  int     region_id;
  QString region;
  QString district;
  QString dsd_coord;
  QString dsd_coord_tel;
  QString education_coord;
  QString education_coord_tel;

  RegionDetails(){
    clear();
  }

  void clear()
  {
    region_id = 0;
    region.clear();
    district.clear();
    dsd_coord.clear();
    dsd_coord_tel.clear();
    education_coord.clear();
    education_coord_tel.clear();
  }
};

struct SchoolDetails{
  int id;
  int region_id;
  QString name;
  QString address;
  QString contact_person_name;
  QString contact_person_surname;
  QString contact_person_tel;
  int num_of_learners;
  int num_of_learners_enrolled;

  SchoolDetails(){
    clear();
  }

  void clear()
  {
    id                        = 0;
    region_id                 = 0;
    num_of_learners           = 0;
    num_of_learners_enrolled  = 0;
    name.clear();
    address.clear();
    contact_person_name.clear();
    contact_person_surname.clear();
  }
};


struct DistributionActivityDetails{
  int           id;
  int           recipient_id;
  int           user_id;
  packReceiver  accepted_by;
  QDate         date;
};

struct RegionDeliveries{
  int           id;
  QString       region;
  QString       district;
  int           count;
};


struct SchoolDeliveries{
  int           id;
  QString       school;
  int           count;
};

struct DistributionActivitySummary{
  int           id;
  int           region_id;
  int           school_id;
  int           packs_rcvd_by_self_count;
  int           packs_rcvd_by_authorised_count;
  int           packs_rcvd_by_issuer_count;
  int           recipients_issued_to_count;
  int           packs_count;


  void clear()
  {
    id                              = 0;
    region_id                       = 0;
    school_id                       = 0;
    packs_rcvd_by_self_count        = 0;
    packs_rcvd_by_authorised_count  = 0;
    packs_rcvd_by_issuer_count      = 0;
    recipients_issued_to_count      = 0;
    packs_count                     = 0;
  }
};

#endif // SYSTEMSHARE_H
