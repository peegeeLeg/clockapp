#ifndef USERADMINISTRATION_H
#define USERADMINISTRATION_H

#include <QDialog>
#include <system_user.h>
#include <SystemShare.h>
#include <database_controller.h>
#include <fingerprintscanner.h>

namespace Ui {
class UserAdministration;
}

class UserAdministration : public QDialog
{
  Q_OBJECT

public:
  explicit UserAdministration(QWidget *parent = 0);
  ~UserAdministration();

  /*!
   * \brief setAdminTypeScreen
   * \param newUser if true, search functionality else new user register
   */
  void setAdminTypeScreen(adminAction action);

  /*!
   * \brief UserAdministration::addSystemUser
   * \return true if sys user added successfully
   *         false if sys user failed to be added
   */
  bool addSystemUser();

  /*!
   * \brief UserAdministration::editSystemUser
   * \return true if sys user editted successfully
   *         false if sys user failed to be editted
   */
  bool editSystemUser();

  /*!
   * \brief UserAdministration::deleteSystemUser
   * \return true if sys user deleted successfully
   *         false if sys user failed to be deleted
   */
  bool deleteSystemUser();

  /*!
   * \brief getAllSystemUsers
   * \param users_list
   */
  void getAllSystemUsers(QList<QPair<UserDetails, int> > &users_list);

  /*!
   * \brief clear contents of screenScreen
   */
  void clearScreen();

private slots:
  /*!
   * \brief on_ua_pb_search_clicked
   */
  void on_ua_pb_search_clicked();

  /*!
   * \brief on_ua_pb_cancel_clicked
   */
  void on_ua_pb_cancel_clicked();

  /*!
   * \brief on_ua_pb_updateAndClose_clicked
   */
  void on_ua_pb_updateAndClose_clicked();

  void on_ua_pb_delete_clicked();

  void on_ua_list_system_users_clicked(const QModelIndex &index);

  void on_ua_pb_biometric_capture_clicked();

private:
  Ui::UserAdministration *ui;

  // bool
  bool new_user;

  adminAction currentAdminAction;

  QList< QPair<UserDetails, int> >  system_users_list;
  int                               system_users_list_index;

  // scanner access
  FingerprintScanner*               identification_finger_scanner;
  QByteArray                        add_user_template;
  int                               template_quality;


};

#endif // USERADMINISTRATION_H
